#!/bin/bash
set -ue
DOCKER_REGISTRY=$1
IMAGES_LIST="images.list"

truncate "$IMAGES_LIST" -s 0

for info_file in $(find -name ".dockerinfo.json"); do

    IMAGE_TAG=$(jq -r '.image_tag' "$info_file")
    BASE_IMAGE_NAME=$(jq -r '.image_name' "$info_file")
    DOCKER_CONTEXT=$(dirname "$info_file")

    if [ -z "$IMAGE_TAG" ] || [ -z "$BASE_IMAGE_NAME" ]; then
        echo "Supplied empty or non existing value in $info_file" > /dev/stderr
        exit 1;
    fi

    FULL_IMAGE_NAME="$DOCKER_REGISTRY/$BASE_IMAGE_NAME:$IMAGE_TAG"

    docker build -t "$FULL_IMAGE_NAME" "$DOCKER_CONTEXT"
    echo "$FULL_IMAGE_NAME" >> "$IMAGES_LIST"
done